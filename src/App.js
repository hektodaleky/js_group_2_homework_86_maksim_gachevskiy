import React, {Component} from "react";
import "./App.css";
import MainMenu from "./containers/MainMenu/MainMenu";
import {Route, Switch} from "react-router-dom";
import AlbumMenu from "./containers/AlbumMenu/AlbumMenu";
import TrackList from "./containers/TrackList/TrackList";
import Layout from "./containers/Layout/Layout";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import HistoryList from "./containers/HistoryList/HistoryList";
import NewAuthor from "./containers/createItem/NewAuthor/NewAuthor";
import NewAlbum from "./containers/createItem/NewAlbum/NewAlbum";
import NewSong from "./containers/createItem/NewSong/NewSong";
import Admin from "./containers/Admin/Admin";

class App extends Component {
    render() {
        return (

            <Layout>
                <Switch>
                    <Route path="/" exact component={MainMenu}/>
                    <Route path="/albums" component={AlbumMenu}/>
                    <Route path="/tracks" component={TrackList}/>
                    <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                    <Route path="/history" exact component={HistoryList}/>
                    <Route path="/create-artist" exact component={NewAuthor}/>
                    <Route path="/create-album" exact component={NewAlbum}/>
                    <Route path="/create-song" exact component={NewSong}/>
                    <Route path="/admin" exact component={Admin}/>
                </Switch>
            </Layout>
        );
    }
}

export default App;
