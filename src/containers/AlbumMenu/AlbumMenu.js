import React, {Component} from "react";
import {connect} from "react-redux";
import {CardColumns} from "reactstrap";
import ListItem from "../../components/ListItem/ListItem";
import {getAlbums} from "../../store/action/album";
import {getTracks} from "../../store/action/track";
import parser from "../../parser";
class AlbumMenu extends Component {


    componentDidMount() {
        if (!this.props.isLoaded)
            this.props.getAlbums(parser(window.location.search).artist.toLowerCase());
    }

    render() {
        return (<CardColumns>
            {
                this.props.albums.map(album => {
                    return <ListItem key={album._id}
                                     about={album.year}
                                     name={album.name}
                                     image={album.image}
                                     id={album._id}
                                     click={() => this.props.getTracks(album.name) }
                                     buttonName="Show Tracks"
                    />
                })
            }
        </CardColumns>)
    }

}
const mapStateToProps = state => ({
    albums: state.albums.albums,
    isLoaded: state.albums.loaded
});

const mapDispatchToProps = dispatch => ({
    getAlbums: (name) => dispatch(getAlbums(name)),
    getTracks: (album) => dispatch(getTracks(album))
});

export default connect(mapStateToProps, mapDispatchToProps)(AlbumMenu);