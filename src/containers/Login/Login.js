import React, {Component, Fragment} from "react";

import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import {connect} from "react-redux";
import Alert from "react-bootstrap/es/Alert";
import FormElement from "../../components/UI/Form/FormElement";
import {loginUser} from "../../store/action/user";


class Login extends Component {
    state = {
        name: '',
        password: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    submitFormHandler = event => {
        event.preventDefault();

        this.props.loginUser(this.state)

    };


    render() {

        return (
            <Fragment>
                <PageHeader>Login</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>
                    {
                        this.props.error
                        && <Alert bsStyle="danger">{this.props.error.errors}</Alert>
                    }

                    <FormElement propertyName="name"
                                 title="Username"
                                 placeholder="Enter username"
                                 type="text"
                                 value={this.state.name}
                                 changeHandler={this.inputChangeHandler}
                                 autoComplete="current-username"
                    />

                    <FormElement propertyName="password"
                                 title="Password"
                                 placeholder="Enter password"
                                 type="password"
                                 value={this.state.password}
                                 changeHandler={this.inputChangeHandler}
                                 autoComplete="current-password"
                    />

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button
                                bsStyle="primary"
                                type="submit"
                            >Log In</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }

}
const mapStateToProps = state => ({
    error: state.users.loginError
});

const mapDispatchToProps = dispatch => ({
    loginUser: userData => dispatch(loginUser(userData))
});


export default connect(mapStateToProps, mapDispatchToProps)(Login);
