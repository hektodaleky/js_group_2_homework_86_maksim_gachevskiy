import React, {Fragment} from "react";
import Toolbar from "../../components/UI/Toolbar/Toolbar";
import {connect} from "react-redux";
import {logoutUser} from "../../store/action/user";
import {NotificationContainer} from "react-notifications";

const Layout = props => {
    return (
        <Fragment>
            <NotificationContainer/>
            <header><Toolbar logout={props.logoutUser} user={props.user}/></header>
            <main className="container">
                {props.children}
            </main>
        </Fragment>
    )
};


const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch=>({
    logoutUser: ()=>dispatch(logoutUser())
});

export default connect(mapStateToProps,mapDispatchToProps)(Layout);