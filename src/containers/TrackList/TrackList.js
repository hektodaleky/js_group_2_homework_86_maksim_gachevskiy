import React, {Component} from "react";
import {connect} from "react-redux";
import ListItem from "../../components/ListItem/ListItem";
import {addTrackInHistory, getTracks} from "../../store/action/track";
import parser from "../../parser";


class TrackList extends Component {
    componentDidMount() {
        if (!this.props.isLoaded)
            this.props.getTracks(parser(window.location.search).album);
    }

    addTrack = (track, user) => {
        if (!user) {
            alert("Авторизуйтесь прежде чем добавить композицию");
            this.props.history.push({
                pathname: '/login'
            });
        }
        else
            this.props.addHistory({track_id: track}, user.token)
    };

    render() {

        return (<div>
            {
                this.props.tracks.map(track => {
                    return <ListItem key={track._id}
                                     about={track.length}
                                     name={track.name}
                                     num={track.num}
                                     image='hide'
                                     id={track._id}
                                     click={() => this.addTrack(track._id, this.props.userToken) }
                                     buttonName="Add Track"
                    />
                })
            }
        </div>)
    }
}


const mapStateToProps = state => ({
    tracks: state.tracks.tracks,
    userToken: state.users.user,
    isLoaded: state.tracks.loaded,
});

const mapDispatchToProps = dispatch => ({
    addHistory: (track, token) => dispatch(addTrackInHistory(track, token)),
    getTracks: (album) => dispatch(getTracks(album))
});

export default connect(mapStateToProps, mapDispatchToProps)(TrackList);