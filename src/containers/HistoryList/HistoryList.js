import React, {Component} from "react";
import {connect} from "react-redux";
import {getHistory} from "../../store/action/track";
import ListItem from "../../components/ListItem/ListItem";



class HistoryList extends Component {
    componentDidMount() {
        if (!this.props.user)
            this.props.history.push({
                pathname: '/login'
            });
        else
            this.props.getHistory(this.props.user.token)

    };

    render() {
        return (
            <div>{
                this.props.historyTrack.map(track => {
                    return <ListItem key={track._id}
                                     about={track.track_id.name}
                                     name={track.track_id.album.author.name}
                                     image='hide'
                                     id={track._id}
                                     optionalField={track.date}
                    />
                })
            }</div>
        )
    }
}
;

const mapStateToProps = state => ({
    user: state.users.user,
    historyTrack: state.tracks.userHistory
});

const mapDispatchToProps = dispatch => ({
    getHistory: (token) => dispatch(getHistory(token))
});

export default connect(mapStateToProps, mapDispatchToProps)(HistoryList)