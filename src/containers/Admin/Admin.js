import React, {Component, Fragment} from "react";
import {approveArtist, deleteArtist, getFalseArtists} from "../../store/action/artist";
import {connect} from "react-redux";
import ListItem from "../../components/ListItem/ListItem";
import {approveAlbum, deleteAlbum, getFalseAlbum} from "../../store/action/album";
import {approveTrack, deleteTracks, getFalseTrack} from "../../store/action/track";

class Admin extends Component {
    componentDidMount() {
        this.props.getFalseArtists();
        this.props.getFalseAlbum();
        this.props.getFalseTracks()
    }

    render() {

        return (
            <Fragment>


                    <h3>Исполнители</h3>
                    {
                        this.props.falseArtists.length>0?this.props.falseArtists.map(artist => {
                            return <ListItem key={artist._id}
                                             about={artist.about}
                                             name={artist.name}
                                             image={artist.image}
                                             id={artist._id}
                                             click={() => this.props.approveArtist(artist._id) }
                                             buttonName="approve"
                                             delete={()=>this.props.deleteArtist(artist._id)}
                                             public={artist.published}
                            />
                        }):<p style={{padding:"30px", fontSize:"25px"}}>Пусто</p>
                    }
                <h3>Альбомы</h3>
                {
                    this.props.falseAlbum.length>0?this.props.falseAlbum.map(album => {
                        return <ListItem key={album._id}
                                         about={album.about}
                                         name={album.name}
                                         image={album.image}
                                         id={album._id}
                                         click={() => this.props.approveAlbum(album._id) }
                                         buttonName="approve"
                                         delete={()=>this.props.deleteAlbum(album._id)}
                                         public={album.published}
                        />
                    }):<p style={{padding:"30px", fontSize:"25px"}}>Пусто</p>
                }
                <h3>Песни</h3>
                <div>
                    {
                        this.props.falseTracks.length>0?this.props.falseTracks.map(track => {
                            return <ListItem key={track._id}
                                             about={track.length}
                                             name={track.name}
                                             num={track.num}
                                             image='hide'
                                             id={track._id}
                                             click={() => this.props.approveTrack(track._id) }
                                             buttonName="Approve"
                                             delete={()=>this.props.deleteTrack(track._id)}
                                             public={track.published}
                            />
                        }):<p style={{padding:"30px", fontSize:"25px"}}>Пусто</p>
                    }
                </div>
            </Fragment>)
    }


}
const mapStateToProps = (state) => ({
    falseArtists: state.artists.falseArtists,
    falseAlbum: state.albums.falseAlbum,
    falseTracks: state.tracks.falseTracks
});

const mapDispatchToProps = dispatch => ({
        getFalseArtists: () => dispatch(getFalseArtists()),
        getFalseAlbum: () => dispatch(getFalseAlbum()),
        getFalseTracks: () => dispatch(getFalseTrack()),

        approveArtist: (data) => dispatch(approveArtist(data)),
        approveAlbum: (data) => dispatch(approveAlbum(data)),
        approveTrack: (data) => dispatch(approveTrack(data)),


        deleteArtist: (data) => dispatch(deleteArtist(data)),
        deleteAlbum: (data) => dispatch(deleteAlbum(data)),
        deleteTrack: (data) => dispatch(deleteTracks(data))


    })
;
export default connect(mapStateToProps, mapDispatchToProps)(Admin);