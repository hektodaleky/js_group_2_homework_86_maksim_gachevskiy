import React, {Component, Fragment} from "react";
import {connect} from "react-redux";
import {Button, Col, ControlLabel, Form, FormControl, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../../../components/UI/Form/FormElement";
import {getArtists} from "../../../store/action/artist";
import {postAlbum} from "../../../store/action/album";

class NewAlbum extends Component {
    state = {
        name: '',
        author: '',
        year: '',
        image: ''
    };

    componentDidMount() {
        this.props.getArtists()
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    submitFormHandler = event => {
        event.preventDefault();
        if (!this.state.name && !this.state.author)
            return;

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);

        });
        console.log(formData);
        this.props.postAlbum(formData);
    };

    
    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    render() {
        console.log(this.props.artists);
        return (
            <Fragment>
                <PageHeader>New Album</PageHeader>

                <Form horizontal onSubmit={this.submitFormHandler}>
                    <select onChange={this.inputChangeHandler} name="author" size="6"
                            style={{fontSize: "12px", marginLeft: "100px"}}>
                        {
                            this.props.artists.map(item => {
                                return <option key={item._id} value={item._id}>{item.name}</option>

                            })
                        }
                    </select>

                    <FormElement propertyName="name"
                                 title="name"
                                 placeholder="Enter author name"
                                 type="text"
                                 value={this.state.name}
                                 changeHandler={this.inputChangeHandler}
                                 autoComplete="new-name"/>


                    <FormElement propertyName="year"
                                 title="year"
                                 placeholder="Album Year"
                                 type="text"
                                 value={this.state.year}
                                 changeHandler={this.inputChangeHandler}
                                 autoComplete="new-name"/>

                    <FormGroup controlId="productImage">
                        <Col componentClass={ControlLabel} sm={2}>
                            Image
                        </Col>
                        <Col sm={10}>
                            <FormControl
                                type="file"
                                name="image"
                                onChange={this.fileChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                </Form>
                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button onClick={this.submitFormHandler} bsStyle="primary" type="submit">Save</Button>
                    </Col>
                </FormGroup>

            </Fragment>
        )
    }
}
;
const mapStateToProps = state => ({
    artists: state.artists.artists
});
const mapDispatchToProps = dispatch => ({
    getArtists: () => dispatch(getArtists()),
    postAlbum: data => dispatch(postAlbum(data))
});
export default connect(mapStateToProps, mapDispatchToProps)(NewAlbum)