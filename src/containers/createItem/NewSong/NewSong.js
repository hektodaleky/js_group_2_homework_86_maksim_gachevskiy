import React, {Component, Fragment} from "react";
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../../../components/UI/Form/FormElement";
import {getAllAlbums} from "../../../store/action/album";
import {postTrack} from "../../../store/action/track";

class NewAlbum extends Component {
    state = {
        name: '',
        length: '',
        album: ''
    };

    componentDidMount() {
        this.props.getAllAlbums()
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    submitFormHandler = event => {
        event.preventDefault();
        if (!this.state.name && !this.state.album)
            return;

        this.props.postTrack(this.state);
    };


    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    render() {
        console.log(this.props.albums);
        return (
            <Fragment>
                <PageHeader>New Track</PageHeader>

                <Form horizontal onSubmit={this.submitFormHandler}>
                    <select onChange={this.inputChangeHandler} name="album" size="6"
                            style={{fontSize: "12px", marginLeft: "100px"}}>
                        {
                            this.props.albums.map(item => {
                                return <option key={item._id}
                                               value={item._id}>{`${item.name} (${item.author.name})`}</option>

                            })
                        }
                    </select>

                    <FormElement propertyName="name"
                                 title="name"
                                 placeholder="Enter author name"
                                 type="text"
                                 value={this.state.name}
                                 changeHandler={this.inputChangeHandler}
                                 autoComplete="new-name"/>


                    <FormElement propertyName="length"
                                 title="length"
                                 placeholder="Track length"
                                 type="text"
                                 value={this.state.length}
                                 changeHandler={this.inputChangeHandler}
                                 autoComplete="new-name"/>


                </Form>
                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button onClick={this.submitFormHandler} bsStyle="primary" type="submit">Save</Button>
                    </Col>
                </FormGroup>

            </Fragment>
        )
    }
}
;
const mapStateToProps = state => ({
    albums: state.albums.albums
});
const mapDispatchToProps = dispatch => ({
    getAllAlbums: () => dispatch(getAllAlbums()),
    postTrack: (data) => dispatch(postTrack(data))
});
export default connect(mapStateToProps, mapDispatchToProps)(NewAlbum)