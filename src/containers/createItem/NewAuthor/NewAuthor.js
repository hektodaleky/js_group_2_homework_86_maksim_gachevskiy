import React, {Component, Fragment} from "react";
import {connect} from "react-redux";
import {Button, Col, ControlLabel, Form, FormControl, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../../../components/UI/Form/FormElement";
import {postArtist} from "../../../store/action/artist";

class NewAuthor extends Component {
    state = {
        name: '',
        about: '',
        image: ''
    };
    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    submitFormHandler = event => {
        event.preventDefault();
        if (!this.state.name)
            return;

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);

        });
        console.log(formData)
        this.props.postArtist(formData);
    };
    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    render() {
        return (
            <Fragment>
                <PageHeader>New Artist</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>
                    <FormElement propertyName="name"
                                 title="name"
                                 placeholder="Enter author name"
                                 type="text"
                                 value={this.state.name}
                                 changeHandler={this.inputChangeHandler}
                                 autoComplete="new-name"/>
                </Form>
                <Form horizontal onSubmit={this.submitFormHandler}>
                    <FormElement propertyName="about"
                                 title="about"
                                 placeholder="Enter info about author"
                                 type="text"
                                 value={this.state.about}
                                 changeHandler={this.inputChangeHandler}
                                 autoComplete="new-name"/>
                </Form>
                <FormGroup controlId="productImage">
                    <Col componentClass={ControlLabel} sm={2}>
                        Image
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="file"
                            name="image"
                            onChange={this.fileChangeHandler}
                        />
                    </Col>
                </FormGroup>
                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button onClick={this.submitFormHandler} bsStyle="primary" type="submit">Save</Button>
                    </Col>
                </FormGroup>

            </Fragment>
        )
    }
}
;
const mapDispatchToProps = dispatch => ({
    postArtist: (data) => dispatch(postArtist(data))
});
export default connect(null, mapDispatchToProps)(NewAuthor)