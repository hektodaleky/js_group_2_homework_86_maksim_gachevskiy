import React, {Component} from "react";
import {getArtists} from "../../store/action/artist";
import {connect} from "react-redux";
import ListItem from "../../components/ListItem/ListItem";
import {getAlbums} from "../../store/action/album";
import {CardColumns} from "reactstrap";

class MainMenu extends Component {
    componentDidMount() {
        this.props.getArtists();

    };

    chooseArtist = (name) => {
        this.props.getAlbums(name)
    };

    render() {
        return (<CardColumns>
            {
                this.props.artists.map(artist => {
                    return <ListItem key={artist._id}
                                     about={artist.about}
                                     name={artist.name}
                                     image={artist.image}
                                     id={artist._id}
                                     click={() => this.chooseArtist(artist.name) }
                                     buttonName="Show Albums"
                    />
                })
            }
        </CardColumns>)
    }
}
;
const mapStateToProps = (state) => ({
    artists: state.artists.artists
});

const mapDispatchToProps = dispatch => ({
    getArtists: () => dispatch(getArtists()),
    getAlbums: (name) => dispatch(getAlbums(name))
});
export default connect(mapStateToProps, mapDispatchToProps)(MainMenu);