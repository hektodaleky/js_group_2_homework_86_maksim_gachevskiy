export const saveState = state => {
    console.log("Save");
    try {
        const serializeState = JSON.stringify(state);
        localStorage.setItem('state', serializeState);
    }
    catch (e) {
        console.log('Could not save state');
    }
};

export const loadState = () => {
    console.log("Load");
    try {
        const serializedState = localStorage.getItem('state');
        if (serializedState === null) {
            return undefined;
        }

        return JSON.parse(serializedState);
    } catch (e) {
        return undefined;
    }
};
