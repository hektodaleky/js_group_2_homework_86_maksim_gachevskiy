import {GET_ALBUM_FAILURE, GET_ALBUM_SUCCESS, GET_FALSE_ALBUM_SUCCESS} from "../action/actionTypes";
const initialState = {
    albums: [],
    error: null,
    loaded: false,
    falseAlbum: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALBUM_SUCCESS:
            return {...state, albums: action.albums, error: false, loaded: true};
        case GET_ALBUM_FAILURE:
            return {...state, error: true};
        case GET_FALSE_ALBUM_SUCCESS:
            return {...state, falseAlbum: action.album};
        default:
            return state
    }

};
export default reducer;