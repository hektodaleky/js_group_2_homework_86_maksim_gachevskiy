import {GET_ARTIST_FAILURE, GET_ARTIST_SUCCESS, GET_FALSE_ARTIST_SUCCESS} from "../action/actionTypes";
const initialState = {
    artists: [],
    error: false,
    loaded:false,
    falseArtists:[]
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ARTIST_SUCCESS:
            return {...state, artists: action.artists, error: false,loaded:true};
        case GET_ARTIST_FAILURE:
            return {...state, error: true};
        case GET_FALSE_ARTIST_SUCCESS:
            return {...state, falseArtists: action.artists};

        default:
            return state

    }
};

export default reducer;
