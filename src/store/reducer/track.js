import {
    GET_FALSE_TRACK_SUCCESS,
    GET_HISTORY_SUCCESS,
    GET_TRACK_FAILURE,
    GET_TRACK_SUCCESS
} from "../action/actionTypes";
const initialState = {
    tracks: [],
    error: null,
    userHistory: [],
    loaded: false,
    falseTracks: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_TRACK_SUCCESS:
            return {...state, tracks: action.tracks, error: false, loaded: true};
        case GET_FALSE_TRACK_SUCCESS:
            return {...state, falseTracks: action.tracks};
        case GET_TRACK_FAILURE:
            return {...state, error: true};
        case GET_HISTORY_SUCCESS:
            return {...state, userHistory: action.history}

        default:
            return state
    }

};
export default reducer;