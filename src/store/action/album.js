import {GET_ALBUM_FAILURE, GET_ALBUM_SUCCESS, GET_FALSE_ALBUM_SUCCESS} from "./actionTypes";
import {push} from "react-router-redux";
import axios from '../../axios-api';


export const getAlbumSuccess = albums => {
    return {type: GET_ALBUM_SUCCESS, albums};
};

export const getAlbumFailure = error => {
    return {type: GET_ALBUM_FAILURE, error};
};

export const getFalseAlbumSuccess = album => {
    return {type: GET_FALSE_ALBUM_SUCCESS, album};
};
export const getAlbums = (artistName) => {
    return dispatch => {
        return axios.get('/albums?artist='+artistName.toLowerCase()).then(
            response => {
                dispatch(getAlbumSuccess(response.data));
                dispatch(push({
                    pathname: '/albums',
                    search: '?artist=' + artistName

                }));

            },
            error => {
                dispatch(getAlbumFailure(error))

            })
    }
};


export const getAllAlbums = () => {
    return dispatch => {
        return axios.get('/albums').then(
            response => {
                dispatch(getAlbumSuccess(response.data));

            },
            error => {
                dispatch(getAlbumFailure(error))

            })
    }
};

export const getFalseAlbum = () => {
    return (dispatch,getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};

        return axios.get('/albums/admin', {headers}).then(
            response => {
                dispatch(getFalseAlbumSuccess(response.data));

            },
            error => {
                console.log(error)


            })
    }
};


export const postAlbum = data=>{
    return dispatch =>{
        return axios.post('/albums',data).then(
            response => {
                console.log(response);
                dispatch(push('/'))
            },
            error => console.log(error)
        )
    }
}

export const approveAlbum = data => {
    return dispatch => {
        return axios.post('/albums/approve',{id:data}).then(
            response => {
                dispatch(getFalseAlbum())
            },
            error => console.log(error)
        )
    }
};


export const deleteAlbum = (data) => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};

        return axios.delete('/albums/' + data, {headers}).then(
            response => {
                dispatch(getFalseAlbum());

            },
            error => {
                console.log(error)


            })
    }
};