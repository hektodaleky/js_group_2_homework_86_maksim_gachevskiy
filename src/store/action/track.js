import {push} from "react-router-redux";
import axios from "../../axios-api";
import {
    GET_FALSE_TRACK_SUCCESS,
    GET_HISTORY_FAILURE,
    GET_HISTORY_SUCCESS,
    GET_TRACK_FAILURE,
    GET_TRACK_SUCCESS
} from "./actionTypes";


export const getTrackSuccess = tracks => {
    return {type: GET_TRACK_SUCCESS, tracks};
};

export const getTrackFailure = error => {
    return {type: GET_TRACK_FAILURE, error};
};

export const getHistorySuccess = history => {
    return {type: GET_HISTORY_SUCCESS, history};
};

export const getHistoryFailure = error => {
    return {type: GET_HISTORY_FAILURE, error};
};
export const getFalseTrackSuccess = tracks => {
    console.log(tracks, "ACTION");
    return {type: GET_FALSE_TRACK_SUCCESS, tracks};
};
export const getTracks = (album) => {
    return dispatch => {
        return axios.get('tracks?album=' + album.toLowerCase()).then(
            response => {
                dispatch(getTrackSuccess(response.data));
                dispatch(push({
                    pathname: '/tracks',
                    search: '?album=' + album

                }));

            },
            error => {
                dispatch(getTrackFailure(error))

            })
    }
};

export const getFalseTrack = () => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};

        return axios.get('/tracks/admin', {headers}).then(
            response => {
                dispatch(getFalseTrackSuccess(response.data));

            },
            error => {
                console.log(error)


            })
    }
};

export const postTrack = data => {
    return dispatch => {
        return axios.post('/tracks', data).then(
            response => {
                console.log(response);
                dispatch(push('/'))
            },
            error => console.log(error)
        )

    }
};


export const addTrackInHistory = (track, token) => {
    return dispatch => {
        axios.post('/track_history', track, {headers: {'Token': token}})
            .then(response => {
            });
    }
};

export const getHistory = token => {
    return dispatch => {
        axios.get('/track_history', {headers: {'Token': token}})
            .then(response => {
                dispatch(getHistorySuccess(response.data));

            });
    }
};

export const approveTrack = data => {
    return dispatch => {
        return axios.post('/tracks/approve',{id:data}).then(
            response => {
                dispatch(getFalseTrack())
            },
            error => console.log(error)
        )
    }
};


export const deleteTracks = (data) => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};

        return axios.delete('/tracks/' + data, {headers}).then(
            response => {
                dispatch(getFalseTrack());

            },
            error => {
                console.log(error)


            })
    }
};