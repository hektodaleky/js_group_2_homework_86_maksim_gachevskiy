import {GET_ARTIST_FAILURE, GET_ARTIST_SUCCESS, GET_FALSE_ARTIST_SUCCESS} from "./actionTypes";
import axios from "../../axios-api";
import {push} from "react-router-redux";

export const getArtistSuccess = artists => {
    return {type: GET_ARTIST_SUCCESS, artists};
};

export const getFalseArtistSuccess = artists => {
    return {type: GET_FALSE_ARTIST_SUCCESS, artists};
};

export const getArtistFailure = error => {
    return {type: GET_ARTIST_FAILURE, error};
};


export const getArtists = () => {
    return dispatch => {
        return axios.get('/artists').then(
            response => {
                dispatch(getArtistSuccess(response.data));

            },
            error => {
                dispatch(getArtistFailure(error))

            })
    }
};

export const getFalseArtists = () => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};

        return axios.get('/artists/admin', {headers}).then(
            response => {
                dispatch(getFalseArtistSuccess(response.data));

            },
            error => {
                console.log(error)


            })
    }
};


export const postArtist = data => {
    return dispatch => {
        return axios.post('/artists', data).then(
            response => {
                console.log(response);
                dispatch(push('/'))
            },
            error => console.log(error)
        )
    }
};

export const approveArtist = data => {
    return dispatch => {
        return axios.post('/artists/approve', {id: data}).then(
            response => {
                dispatch(getFalseArtists())
            },
            error => console.log(error)
        )
    }
};


export const deleteArtist = (data) => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};

        return axios.delete('/artists/' + data, {headers}).then(
            response => {
                dispatch(getFalseArtists());

            },
            error => {
                console.log(error)


            })
    }
};