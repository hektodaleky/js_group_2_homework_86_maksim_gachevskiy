import thunkMiddleware from "redux-thunk";
import createHistory from "history/createBrowserHistory";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {routerMiddleware} from "react-router-redux";

import {loadState, saveState} from "./localStorage";

import usersReducer from "./reducer/user";
import artistReducer from "./reducer/artist";
import albumReducer from "./reducer/album";
import trackReducer from "./reducer/track";

const rootReducer = combineReducers({

    users: usersReducer,
    artists: artistReducer,
    albums: albumReducer,
    tracks: trackReducer
});
export const history = createHistory();
const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancers = composeEnhancers(applyMiddleware(...middleware));


const persistedState = loadState();
const store = createStore(rootReducer, persistedState, enhancers);
store.subscribe(() => {
    saveState({
        users: {
            user: store.getState().users.user
        }
    })
});


export default store;