import React from 'react';

import {LinkContainer} from "react-router-bootstrap";
import {Nav, NavItem} from "react-bootstrap";

const UserMenu=({user,logout})=>(
    <Nav>
        <LinkContainer to="/profile" exact>
            <NavItem>Hello, {user}</NavItem>
        </LinkContainer>
        <LinkContainer to="/logout" exact>
            <NavItem onClick={logout}>Logout</NavItem>
        </LinkContainer>
        <LinkContainer to="/history" exact>
            <NavItem>Track History</NavItem>
        </LinkContainer>
    </Nav>
);

export default UserMenu;