import React,{Fragment} from "react";
import {Nav, Navbar, NavItem, SplitButton} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import AnonymousMenu from "./Menus/AnonymousMenu";
import UserMenu from "./Menus/UserMenu";

const Toolbar = ({user, logout}) => (
    <Navbar>
        <Navbar.Header>
            <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse>
            <Nav>
                <LinkContainer to="/" exact>
                    <NavItem>All Artists</NavItem>
                </LinkContainer>
                {user ? <UserMenu logout={logout} user={user.name}/> : <AnonymousMenu />}
                {user ?<Fragment>
                    <LinkContainer to="/create-artist" exact>
                        <NavItem>Add Artist</NavItem>
                    </LinkContainer>
                    <LinkContainer to="/create-album" exact>
                        <NavItem>Add Album</NavItem>
                    </LinkContainer>
                    <LinkContainer to="/create-song" exact>
                        <NavItem>Add Song</NavItem>
                    </LinkContainer></Fragment>
                 : null}
                {user&&user.role === 'admin' ? <LinkContainer to="/admin" exact>
                    <NavItem>Admin Panel</NavItem>
                </LinkContainer> : null}


            </Nav>
        </Navbar.Collapse>
    </Navbar>
);

export default Toolbar;