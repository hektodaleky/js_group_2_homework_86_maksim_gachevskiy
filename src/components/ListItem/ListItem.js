import React from "react";
import { Card, CardBody, CardImg, CardSubtitle, CardText, CardTitle} from "reactstrap";
import PropTypes from "prop-types";

import config from "../../config";

import notFound from "../../assets/images/not-found.png";
import {Button} from "react-bootstrap";

const ListItem = props => {
    let image = notFound;

    if (props.image) {
        image = config.apiUrl + 'uploads/' + props.image;
    }

    return (



        <Card sm="6">
            {props.image !== 'hide' ? <CardImg top width="100%" src={image} alt="Artist Image"
                                               style={{width: '100px', marginRight: '10px'}}/> : null}
            <CardBody>
                <CardTitle> {props.num ? props.num + ". " + props.name : props.name}</CardTitle>
                <CardText>{props.about}</CardText>
                {!props.public?
                    props.buttonName ? <Button bsStyle="success"
                                                bsSize="large"
                                                onClick={props.click}>{props.buttonName}</Button> : null:null}
                {props.optionalField ? <CardSubtitle>{props.optionalField}</CardSubtitle> : null}
                {props.delete ? <Button onClick={props.delete} bsStyle="danger" bsSize="large" active>x</Button> : null}
            </CardBody>
        </Card>



    );
};

ListItem.propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    image: PropTypes.string,
    click: PropTypes.func,
    buttonName: PropTypes.string,
    num: PropTypes.number,
    optionalField: PropTypes.string,
    delete: PropTypes.func
};

export default ListItem;